const { Router } = require('express');
const app = Router();

const Jobs = require('../controllers/jobs/jobs');

//Jobs routes
app.get('/jobs', Jobs.index);
app.post('/jobs', Jobs.create)
app.put('/jobs/:id', Jobs.update);
app.delete('/jobs/:id', Jobs.delete);
app.get('/jobs/:id', Jobs.findBy)


module.exports = app;