const mongoose = require('mongoose');
const Jobs = require('../../Models/Jobs');

const controllers = {
    index: (req, res) => {
        Jobs
            .find()
            .exec()
            .then(data => {
                res
                    .json({
                        type: 'Reading Jobs',
                        data: data
                    })
                    .status(200)
            })
            .catch(err => {
                console.log(`caugth error: ${err}`);
                return res.status(500).json(err)
            })
    },
    create: (req, res) => {
        const newJob = new Jobs({
            _id: mongoose.Types.ObjectId(),
            description: req.body.description,
            hiringDate: req.body.hiringDate,
            salary: req.body.salary,
            location: req.body.location,
            contactEmail: req.body.contactEmail,
            isStillAvailable: req.body.isStillAvailable

        });
        newJob
            .save()
            .then(data => {
                res
                    .json({
                        type: 'Job Created',
                        data: data
                    })
                    .status(200)
            })
            .catch(err => {
                console.log(`caugth error: ${err}`);
                return res.status(500).json(err);
            })
    },
    update: (req, res) => {
        Jobs
            .updateOne({ _id: req.params.id }, {
                description: req.body.description,
                hiringDate: req.body.hiringDate,
                salary: req.body.salary,
                location: req.body.location,
                contactEmail: req.body.contactEmail,
                isStillAvailable: req.body.isStillAvailable
            })
            .then(data => {
                res
                    .json({
                        type: 'Updating Jobs',
                        data: data
                    })
                    .status(200)
            })
            .catch(err => {
                console.log(`caugth error: ${err}`);
                return res.status(500).json(err);
            })
    },
    delete: (req, res) => {
        Jobs
            .deleteOne({ _id: req.params.id })
            .then(data => {
                res
                    .json({
                        type: 'Deleting One register job',
                        data: data
                    })
                    .status(200)
            })
            .catch(err => {
                console.log(`caugth error: ${err}`);
                return res.status(500).json(err);
            })
    },
    findBy: (req, res) => {
        //utilizamos el mètodo findById
        Jobs
            .findById(req.params.id)
            .then(data => {
                res
                    .json({
                        type: 'Id finded',
                        data: data
                    })
                    .status(200)
            })
            .catch(err => {
                console.log(`caugth error: ${err}`);
                return res.status(500).json(err);
            })
    }
}

module.exports = controllers;

