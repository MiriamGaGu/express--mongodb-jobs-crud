const mongoose = require('mongoose');


const Schema = new mongoose.Schema({
    //crea ID de manera automatica
    _id: mongoose.Schema.Types.ObjectId,
    description: { type: String, required: true },
    hiringDate: { type: Date, default: Date.now },
    salary: mongoose.Schema.Types.Decimal128,
    location: { type: String, required: true },
    contactEmail: { type: String, required: true },
    isStillAvailable: { type: Boolean, default: true }
});

module.exports = mongoose.model('Jobs', Schema)